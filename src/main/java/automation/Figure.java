package automation;

public abstract class Figure {
    private double sideLength;

    public Figure (double sideLength){

        if(sideLength > 0) {
            this.sideLength = sideLength;
        } else throw new IllegalArgumentException("Length must be positive");
    }

    public double getSideLength() {
        return sideLength;
    }

    public abstract double getPerimeter();
    public abstract double getArea();
}

package automation;

public class Circle extends Figure{

    public Circle(double sideLength) {
        super(sideLength);
    }

    @Override
    public double getPerimeter() {
        return 2 * this.getSideLength() * Math.PI;
    }

    @Override
    public double getArea() {
        return Math.pow(this.getSideLength(), 2) * Math.PI;
    }
}

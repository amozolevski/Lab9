package automation;

public class Square extends Figure{

    public Square(double sideLength) {
        super(sideLength);
    }

    @Override
    public double getPerimeter() {
        return 4 * this.getSideLength();
    }

    @Override
    public double getArea() {
        return Math.pow(this.getSideLength(), 2);
    }
}

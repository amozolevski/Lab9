package automationTest;

import automation.Circle;
import automation.Square;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class ApplicationTest {
    Circle circle = new Circle(4);
    Square square = new Square(4);

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void circleRadiusExcTestCase(){
        Circle circle = new Circle(-3);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void squareLengthExcTestCase(){
        Square square = new Square(0);
    }

    @Test
    public void getCirclePerimeterTestCase(){
        Assert.assertEquals(8*Math.PI, circle.getPerimeter());
    }

    @Test
    public void getSquarePerimeterTestCase(){
        Assert.assertEquals(16.0, square.getPerimeter());
    }

    @Test
    public void getCircleAreaTestCase(){
        double target = Math.pow(4, 2) * Math.PI;
        Assert.assertEquals(target, circle.getArea());
    }

    @Test
    public void getSquareAreaTestCase(){
        Assert.assertEquals(Math.pow(4, 2), square.getArea());
    }

    @DataProvider(name = "CirclePerimeterTestCase")
    public Object[][] createCirclePerimeterTestCase(){
        return new Object[][]{{10.0},
                            {12.0},
                            {8*Math.PI},
                            {14.0}};
    }

    @Test(dataProvider = "CirclePerimeterTestCase")
    public void circlePerimeterTestCase(double number){
        Assert.assertEquals(number, circle.getPerimeter());
    }
}
